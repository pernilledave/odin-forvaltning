﻿$axure.loadDocument({
  "configuration":{
    "showPageNotes":false,
    "showPageNoteNames":false,
    "showAnnotations":false,
    "linkStyle":"b",
    "linkFlowsToPages":true,
    "linkFlowsToPagesNewWindow":true,
    "hideAddress":false,
    "preventScroll":false,
    "useLabels":false,
    "enabledViewIds":["a",
"c"],
    "loadFeedbackPlugin":false},
  "sitemap":{
    "rootNodes":[{
        "pageName":"Hjem",
        "type":"Wireframe",
        "url":"hjem.html",
        "children":[{
            "pageName":"Våre fond (alle fond)",
            "type":"Wireframe",
            "url":"v_re_fond__alle_fond_.html",
            "children":[{
                "pageName":"*ODIN Norden",
                "type":"Wireframe",
                "url":"_odin_norden.html"}]},
{
            "pageName":"*Avkastningsoversikt",
            "type":"Wireframe",
            "url":"_avkastningsoversikt.html",
            "children":[{
                "pageName":"*Aksjefond",
                "type":"Wireframe",
                "url":"_aksjefond.html"}]},
{
            "pageName":"Rapporter",
            "type":"Wireframe",
            "url":"rapporter.html",
            "children":[{
                "pageName":"*Fondskommentarer",
                "type":"Wireframe",
                "url":"_fondskommentarer.html"}]}]}]},
  "globalVariables":{
    "onloadvariable":""},
  "defaultAdaptiveView":{
    "name":"",
    "size":{
      "width":0,
      "height":0},
    "condition":"<="},
  "adaptiveViews":[{
      "id":"a",
      "name":"Portrait Phone",
      "size":{
        "width":320,
        "height":0},
      "condition":"<="},
{
      "id":"b",
      "name":"Portrait Tablet",
      "baseViewId":"a",
      "size":{
        "width":768,
        "height":0},
      "condition":"<="},
{
      "id":"c",
      "name":"Large Display",
      "baseViewId":"a",
      "size":{
        "width":1200,
        "height":0},
      "condition":">="}],
  "stylesheet":{
    "defaultStyles":{
      "buttonShape":{
        "id":"cad4f3db2bd4435f8f4e5374709df77f",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"center",
        "verticalAlignment":"middle",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"2",
        "paddingBottom":"2",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "paragraph":{
        "id":"4a4a863a1cc7486cbcf4a9c17056efc2",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"0",
        "paddingBottom":"0",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "h1":{
        "id":"2406bdf6e3214985a96b816962873d62",
        "fontName":"'Chaparral Pro Italic', 'Chaparral Pro'",
        "fontSize":"36px",
        "fontWeight":"400",
        "fontStyle":"italic",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"middle",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"0",
        "paddingBottom":"0",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "h2":{
        "id":"d5ec629aa75f4cc4812e16181fb80db7",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"24px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"0",
        "paddingBottom":"0",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "h3":{
        "id":"330595a89d6946b185c765461a41b531",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"18px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"0",
        "paddingBottom":"0",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "h4":{
        "id":"01e184ce6c2546819db38d3ef4744cd6",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"0",
        "paddingBottom":"0",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "h5":{
        "id":"aed48c2117ef4dfc9a18f9bb163f58d3",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"0",
        "paddingBottom":"0",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "h6":{
        "id":"e059061234224ccfbee6c818ae79c745",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"10px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"0",
        "paddingBottom":"0",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "imageBox":{
        "id":"aacaceda66a24dd89ffbbeed382135e1",
        "fontName":"'Arial'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"center",
        "verticalAlignment":"middle",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF000000,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"2",
        "paddingBottom":"2",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979},
        "linePattern":"none",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "hyperlink":{
        "id":"3c08fa4653f64b20b6ceb3a53bff153f",
        "underline":false,
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF0099FF,
          "opacity":1},
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "limbo":false},
      "hyperlinkMouseOver":{
        "id":"9b07299c78394d0f874cdf6791b4127e"},
      "hyperlinkMouseDown":{
        "id":"50e41842ef1c4ca2a3981adc1b83e16b"},
      "textBox":{
        "id":"faa5c818c0674f9a999c64727d937804",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "stateStyles":{
},
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "textArea":{
        "id":"0c3d57d0fce74c80a7f385d39fad9dda",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "stateStyles":{
},
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "comboBox":{
        "id":"f1d17df9d2bd445ca5f87e28e0931d5b",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF000000,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "listBox":{
        "id":"459a0ea14d1441fe9d413c19073183bd",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "checkbox":{
        "id":"d4d9f0b353fd4615bf461fc8abe630fd",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "radioButton":{
        "id":"d7606fa04ac544258f9795d2c0375994",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "flowShape":{
        "id":"dfffc3b4200a4db2ba0962504e2f34cc",
        "fontName":"'Arial'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"center",
        "verticalAlignment":"middle",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"linearGradient",
          "colors":[{
              "color":0xFFFFFFFF},
{
              "color":0xFFF2F2F2},
{
              "color":0xFFE4E4E4},
{
              "color":0xFFFFFFFF}]},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"2",
        "paddingBottom":"2",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979},
        "linePattern":"solid",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "treeNodeObject":{
        "id":"d2dedba8482840768a3ca4c6ae0e010c",
        "fontName":"'Arial'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "stateStyles":{
},
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "limbo":false,
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979},
        "linePattern":"solid"},
      "button":{
        "id":"097a35c58039473d8d3ac6208fb3a6f3",
        "fontName":"'Franklin Gothic Book'",
        "fontSize":"14px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"center",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF1B1B1B,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "imageMapRegion":{
        "id":"8d46b7959eb84b85b3d4ae0c8f99cd38",
        "fontName":"'Arial'",
        "fontWeight":"400",
        "fontStyle":"normal",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0"},
      "inlineFrame":{
        "id":"f545ef2359354462a82d6f4f9a895c62",
        "fontName":"'Arial'",
        "fontWeight":"400",
        "fontStyle":"normal",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "dynamicPanel":{
        "id":"7cdfdef075114a76880421e2de9be1af",
        "fontName":"'Arial'",
        "fontWeight":"400",
        "fontStyle":"normal",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "referenceDiagramObject":{
        "id":"d2331d39f6c647a7906d64bedd167601",
        "fontName":"'Arial'",
        "fontWeight":"400",
        "fontStyle":"normal",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false},
      "repeater":{
        "id":"09a2da3c3b1744fb9fc1921ba9c585e6",
        "fontName":"'Arial'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"center",
        "verticalAlignment":"middle",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "opacity":"1",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"2",
        "paddingBottom":"2",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "textRotation":"0",
        "borderWidth":"-1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979},
        "linePattern":"solid",
        "cornerRadiusTopLeft":"0",
        "outerShadow":{
          "on":false,
          "offsetX":5,
          "offsetY":5,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.34901961684227}}},
      "table":{
        "id":"1b604ab2858140ed9a269981537896a3",
        "fontName":"'Arial'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"center",
        "verticalAlignment":"middle",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"2",
        "paddingBottom":"2",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979}},
      "tableCell":{
        "id":"54485023534b4f8895690103d8bbfea7",
        "fontName":"'Arial'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"left",
        "verticalAlignment":"top",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "lineSpacing":"0px",
        "stateStyles":{
},
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "textShadow":{
          "on":false,
          "offsetX":1,
          "offsetY":1,
          "blurRadius":5,
          "color":{
            "r":0,
            "g":0,
            "b":0,
            "a":0.647058844566345}},
        "paddingTop":"2",
        "paddingBottom":"2",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "limbo":false,
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979}},
      "menuObject":{
        "id":"8026897fdb984215932eacdcc89404ee",
        "fontName":"'Arial'",
        "fontWeight":"400",
        "fontStyle":"normal",
        "fill":{
          "fillType":"solid",
          "color":0xFFFFFFFF},
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF797979}},
      "horizontalLine":{
        "id":"469739251478489f81b947d923952a00",
        "fontName":"'Arial'",
        "fontWeight":"400",
        "fontStyle":"normal",
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "opacity":"1",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF000000},
        "linePattern":"solid"},
      "verticalLine":{
        "id":"b3288936d7da457a8ef70a21852b5f1f",
        "fontName":"'Arial'",
        "fontWeight":"400",
        "fontStyle":"normal",
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "opacity":"1",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "visible":true,
        "limbo":false,
        "rotation":"0",
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF000000},
        "linePattern":"solid"},
      "connector":{
        "id":"fbe90a4d7b5549e2b64e09d2f6f3f643",
        "fontName":"'Arial'",
        "fontSize":"13px",
        "fontWeight":"400",
        "fontStyle":"normal",
        "underline":false,
        "horizontalAlignment":"center",
        "foreGroundFill":{
          "fillType":"solid",
          "color":0xFF333333,
          "opacity":1},
        "baseStyle":"627587b6038d43cca051c114ac41ad32",
        "location":{
          "x":0,
          "y":0},
        "size":{
          "width":0,
          "height":0},
        "limbo":false,
        "borderWidth":"1",
        "borderFill":{
          "fillType":"solid",
          "color":0xFF0099CC},
        "linePattern":"solid"}},
    "customStyles":{
}}});